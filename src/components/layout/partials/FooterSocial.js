import React from 'react';
import classNames from 'classnames';

const FooterSocial = ({
  className,
  ...props
}) => {

  const classes = classNames(
    'footer-social',
    className
  );

  return (
    <div
      {...props}
      className={classes}
    >
      <ul className="list-reset ">
        <li>
          <a href="https://www.linkedin.com/in/jesse-dennis-86a813179/">
            <svg
              width="75"
              height="75"
              viewBox="0 0 25 25"
              xmlns="http://www.w3.org/2000/svg">
              <title>LinkedIn</title>
              <g>
                <circle cx="12.145" cy="3.892" r="1" />
                <path
                  d="M19 0h-14c-2.761 0-5 2.239-5 5v14c0 2.761 2.239 5 5 5h14c2.762 0 5-2.239 5-5v-14c0-2.761-2.238-5-5-5zm-11 19h-3v-11h3v11zm-1.5-12.268c-.966 0-1.75-.79-1.75-1.764s.784-1.764 1.75-1.764 1.75.79 1.75 1.764-.783 1.764-1.75 1.764zm13.5 12.268h-3v-5.604c0-3.368-4-3.113-4 0v5.604h-3v-11h3v1.765c1.396-2.586 7-2.777 7 2.476v6.759z" />
              </g>
            </svg>
          </a>
        </li>
      </ul>
      <div className="text-xs ta-c">
        LinkedIn
      </div>
    </div>
  );
}

export default FooterSocial;